<?php

namespace Drupal\commerce_variation_transfer;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_variation_transfer\Event\ProductVariationTransferAccessEvent;
use Drupal\commerce_variation_transfer\Event\ProductVariationTransferEvent;
use Drupal\commerce_variation_transfer\Event\ProductVariationTransferEvents;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The product variation transferer service.
 */
class ProductVariationTransferer implements ProductVariationTransfererInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a new ProductVariationTransferer.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritDoc}
   */
  public function transferAccess(ProductVariationInterface $variation, ProductInterface $product) {
    /** @var \Drupal\commerce_product\Entity\ProductTypeInterface $product_type */
    $product_type = $this->entityTypeManager->getStorage('commerce_product_type')->load($product->bundle());

    $access = AccessResult::allowed();

    if (!in_array($variation->bundle(), $product_type->getVariationTypeIds())) {
      $access = $access->orIf(AccessResult::forbidden('The selected product does not reference variations of that type.'));
    }

    if ($variation->getProductId() === $product->id()) {
      $access = $access->orIf(AccessResult::forbidden('The variation already belongs to this product.'));
    }

    if (!$product_type->allowsMultipleVariations() && $product->hasVariations()) {
      $access = $access->orIf(AccessResult::forbidden('The product type does not allow multiple variations.'));
    }

    // Allow other modules to alter access.
    $event = new ProductVariationTransferAccessEvent($variation, $variation->getProduct(), $product, $access);
    /** @var \Drupal\commerce_variation_transfer\Event\ProductVariationTransferAccessEvent $event */
    $event = $this->eventDispatcher->dispatch($event, ProductVariationTransferEvents::PRODUCT_VARIATION_TRANSFER_ACCESS);

    return $event->getAccess();
  }

  /**
   * {@inheritDoc}
   */
  public function transfer(ProductVariationInterface $variation, ProductInterface $product) {
    $previous_product = $variation->getProduct();

    $event = new ProductVariationTransferEvent($variation, $previous_product, $product);
    $this->eventDispatcher->dispatch($event, ProductVariationTransferEvents::PRODUCT_VARIATION_PRETRANSFER);

    // Remove variation from previous product.
    $previous_product->removeVariation($variation);
    $previous_product->save();

    // Add variation to new product.
    $product->addVariation($variation);
    $product->save();

    // Set the variation's reference to new product.
    $variation->product_id = $product->id();
    $variation->save();

    $event = new ProductVariationTransferEvent($variation, $previous_product, $product);
    $this->eventDispatcher->dispatch($event, ProductVariationTransferEvents::PRODUCT_VARIATION_POSTTRANSFER);

    return $variation;
  }

}
