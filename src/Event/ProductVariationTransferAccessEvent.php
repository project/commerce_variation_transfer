<?php

namespace Drupal\commerce_variation_transfer\Event;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Access\AccessResultInterface;

/**
 * Defines the product variation transfer access event.
 *
 * @see \Drupal\commerce_variation_transfer\Event\ProductVariationTransferEvents
 */
class ProductVariationTransferAccessEvent extends ProductVariationTransferEvent {

  /**
   * The access result.
   *
   * @var \Drupal\Core\Access\AccessResultInterface
   */
  protected $access;

  /**
   * Constructs a new ProductVariationTransferAccessEvent.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation
   *   The product variation.
   * @param \Drupal\commerce_product\Entity\ProductInterface $previous_product
   *   The previous product.
   * @param \Drupal\commerce_product\Entity\ProductInterface $new_product
   *   The new product.
   * @param \Drupal\Core\Access\AccessResultInterface $access
   *   The access result.
   */
  public function __construct(ProductVariationInterface $product_variation, ProductInterface $previous_product, ProductInterface $new_product, AccessResultInterface $access) {
    parent::__construct($product_variation, $previous_product, $new_product);
    $this->access = $access;
  }

  /**
   * Gets the access result.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function getAccess() {
    return $this->access;
  }

  /**
   * Sets the access result.
   *
   * @param \Drupal\Core\Access\AccessResultInterface $access
   *   The access result.
   *
   * @return $this
   */
  public function setAccess(AccessResultInterface $access) {
    $this->access = $access;
    return $this;
  }

}
