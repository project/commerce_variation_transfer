<?php

namespace Drupal\commerce_variation_transfer\Event;

/**
 * Defines the events for the commerce_variation_transfer module.
 */
final class ProductVariationTransferEvents {

  /**
   * Name of the event fired before transferring a product variation.
   *
   * @Event
   *
   * @see \Drupal\commerce_variation_transfer\Event\ProductVariationTransferEvent
   */
  const PRODUCT_VARIATION_PRETRANSFER = 'commerce_variation_transfer.commerce_product_variation.pretransfer';

  /**
   * Name of the event fired after transferring a product variation.
   *
   * @Event
   *
   * @see \Drupal\commerce_variation_transfer\Event\ProductVariationTransferEvent
   */
  const PRODUCT_VARIATION_POSTTRANSFER = 'commerce_variation_transfer.commerce_product_variation.posttransfer';

  /**
   * Name of the event fired when checking variation transfer access.
   *
   * @Event
   *
   * @see \Drupal\commerce_variation_transfer\Event\ProductVariationTransferAccessEvent
   */
  const PRODUCT_VARIATION_TRANSFER_ACCESS = 'commerce_variation_transfer.commerce_product_variation.transfer_access';

}
