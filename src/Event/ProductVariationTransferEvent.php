<?php

namespace Drupal\commerce_variation_transfer\Event;

use Drupal\commerce\EventBase;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;

/**
 * Defines the product variation transfer event.
 *
 * @see \Drupal\commerce_variation_transfer\Event\ProductVariationTransferEvents
 */
class ProductVariationTransferEvent extends EventBase {

  /**
   * The product variation.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $productVariation;

  /**
   * The current product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $previousProduct;

  /**
   * The new product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $newProduct;

  /**
   * Constructs a new ProductVariationTransferEvent.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation
   *   The product variation.
   * @param \Drupal\commerce_product\Entity\ProductInterface $previous_product
   *   The previous product.
   * @param \Drupal\commerce_product\Entity\ProductInterface $new_product
   *   The new product.
   */
  public function __construct(ProductVariationInterface $product_variation, ProductInterface $previous_product, ProductInterface $new_product) {
    $this->productVariation = $product_variation;
    $this->previousProduct = $previous_product;
    $this->newProduct = $new_product;
  }

  /**
   * Gets the product variation.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface
   *   The product variation.
   */
  public function getProductVariation() {
    return $this->productVariation;
  }

  /**
   * Gets the previous product the variation belonged to.
   *
   * @return \Drupal\commerce_product\Entity\ProductInterface
   *   The current product.
   */
  public function getPreviousProduct() {
    return $this->previousProduct;
  }

  /**
   * Gets the new product the variation will transfer to.
   *
   * @return \Drupal\commerce_product\Entity\ProductInterface
   *   The new product.
   */
  public function getNewProduct() {
    return $this->newProduct;
  }

}
