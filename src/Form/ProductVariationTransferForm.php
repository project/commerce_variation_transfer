<?php

namespace Drupal\commerce_variation_transfer\Form;

use Drupal\commerce_variation_transfer\ProductVariationTransfererInterface;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The product variation transfer form.
 */
class ProductVariationTransferForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The product variation transferer.
   *
   * @var \Drupal\commerce_variation_transfer\ProductVariationTransfererInterface
   */
  protected $transferer;

  /**
   * The product variation.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variation;

  /**
   * Constructs a new ProductVariationTransferForm object.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_variation_transfer\ProductVariationTransfererInterface $transferer
   *   The product variation transferer.
   */
  public function __construct(CurrentRouteMatch $current_route_match, EntityTypeManagerInterface $entity_type_manager, ProductVariationTransfererInterface $transferer) {
    $this->variation = $current_route_match->getParameter('commerce_product_variation');
    $this->entityTypeManager = $entity_type_manager;
    $this->transferer = $transferer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('commerce_variation_transfer.transferer')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'commerce_product_variation_transfer_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Set the variation here for easy access in form alters.
    $form_state->set('variation', $this->variation);

    $form['transfer_to_product'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Transfer to product'),
      '#description' => $this->t('Select the product to which the variation will transfer.'),
      '#required' => TRUE,
      '#target_type' => 'commerce_product',
      '#selection_handler' => 'default',
      '#selection_settings' => [],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $transfer_to_product_id = $form_state->getValue('transfer_to_product');
    /** @var \Drupal\commerce_product\Entity\ProductInterface $transfer_to_product */
    $transfer_to_product = $this->entityTypeManager->getStorage('commerce_product')->load($transfer_to_product_id);

    if (!$transfer_to_product) {
      $form_state->setErrorByName('transfer_to_product', $this->t('A valid product must be selected.'));
      return;
    }

    $form_state->set('transfer_to_product', $transfer_to_product);
    $transfer_access = $this->transferer->transferAccess($this->variation, $transfer_to_product);
    if (!$transfer_access->isAllowed()) {
      $reason = $this->t('The variation is not allowed to transfer to the selected product.');
      if ($transfer_access instanceof AccessResultReasonInterface) {
        $reason = $transfer_access->getReason();
      }

      $form_state->setErrorByName('transfer_to_product', $reason);
    }

  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $from_product = $this->variation->getProduct();
    $transfer_to_product = $form_state->get('transfer_to_product');

    $this->transferer->transfer($this->variation, $transfer_to_product);

    $this->messenger()->addMessage($this->t('Successfully transferred variation <em>@variation</em> from product <em>@previous_product</em> to product <em>@product</em>', [
      '@variation' => $this->variation->label(),
      '@previous_product' => Link::fromTextAndUrl($from_product->label(), $from_product->toUrl('edit-form'))->toString(),
      '@product' => Link::fromTextAndUrl($transfer_to_product->label(), $transfer_to_product->toUrl('edit-form'))->toString(),
    ]));

    // This will be the new product's variation listing page.
    $form_state->setRedirectUrl($this->variation->toUrl('collection'));
  }

}
