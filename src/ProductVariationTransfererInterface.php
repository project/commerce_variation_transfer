<?php

namespace Drupal\commerce_variation_transfer;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;

/**
 * Provides methods for transferring a product variation to another product.
 */
interface ProductVariationTransfererInterface {

  /**
   * Determines if the variation can transfer to a product.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $variation
   *   The product variation.
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The transfer access result.
   */
  public function transferAccess(ProductVariationInterface $variation, ProductInterface $product);

  /**
   * Transfers the variation to a product.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $variation
   *   The product variation.
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface
   *   The variation after being transferred.
   */
  public function transfer(ProductVariationInterface $variation, ProductInterface $product);

}
