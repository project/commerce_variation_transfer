Commerce Product Variation Transfer
=================

Provides functionality to transfer a product variation to another product in Drupal Commerce.

## Use Case

This module was created to have an easy way to move a product variation to another product without having to re-create or migrate the variation.  The internal ID of the variation is not modified, keeping references to the variation intact for data such as order history and stock transactions.
## Setup

1. Install the module (https://www.drupal.org/docs/develop/using-composer/manage-dependencies#adding-modules)
2. Edit permissions and give "transfer commerce_product_variation access" to users who should be allowed to transfer product variations.
The user must also have access to manage the variation type they intend to transfer.
3. Visit a product's variation listing page (e.g. /product/PRODUCT_ID/variations).
4. Click the operations dropdown and click the "Transfer" link.
5. Search for a product that you wish to transfer the variation to.
6. Click the submit button.  If successful, you will be redirected to the product's variation listing that you transferred the variation to.
7. If the transfer was not successful, you will be shown a message for why the transfer did not go through.  See the restrictions for transferring variations section.

## Transfer Restrictions

1. Variation's may not transfer to the product they already belong to.
2. Variation's may not transfer to a product type that does not allow variations of that type.
3. Variation's may not transfer to a product that only allows one variation that already has one variation.

## Access to Transfer Variations

1. In order to transfer a variation, the user must have the "transfer commerce_product_variation access" permission, as well as permission to manage the variation type.
