<?php

namespace Drupal\Tests\commerce_variation_transfer\Kernel;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * Tests the product variation transfer functionality.
 *
 * @coversDefaultClass \Drupal\commerce_variation_transfer\ProductVariationTransferer
 *
 * @group commerce_variation_transfer
 */
class ProductVariationTransferTest extends CommerceKernelTestBase {

  /**
   * The product variation transferer.
   *
   * @var \Drupal\commerce_variation_transfer\ProductVariationTransfererInterface
   */
  protected $transferer;

  /**
   * The product to test against.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * The product variation to test against.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variation;

  /**
   * The transfer product to test against.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $transferProduct;

  /**
   * The transfer product variation to test against.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $transferVariation;

  /**
   * Enable modules.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_product',
    'commerce_variation_transfer',
    'commerce_variation_transfer_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product');
    $this->installConfig(['commerce_product']);

    $this->transferer = $this->container->get('commerce_variation_transfer.transferer');

    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $product = Product::create([
      'type' => 'default',
      'title' => 'My Product Title',
    ]);
    $product->save();
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = ProductVariation::create([
      'type' => 'default',
      'product_id' => $product->id(),
    ]);
    $variation->save();
    $this->product = $this->reloadEntity($product);
    $this->variation = $this->reloadEntity($variation);

    $transfer_variation_type = ProductVariationType::create([
      'id' => 'transfer',
      'label' => 'Transfer',
      'orderItemType' => 'default',
      'generateTitle' => 'true',
    ]);
    $transfer_variation_type->save();

    // Allows default and transfer variation types.
    $transfer_product_type = ProductType::create([
      'id' => 'transfer',
      'label' => 'Transfer',
      'variationType' => 'transfer',
      'variationTypes' => ['transfer', 'default'],
      'multipleVariations' => FALSE,
      'injectVariationFields' => TRUE,
    ]);
    $transfer_product_type->save();

    $transfer_product = Product::create([
      'type' => 'transfer',
      'title' => 'Transfer',
    ]);
    $transfer_product->save();

    $transfer_variation = ProductVariation::create([
      'type' => 'transfer',
      'product_id' => $transfer_product->id(),
    ]);
    $transfer_variation->save();
    $this->transferProduct = $this->reloadEntity($transfer_product);
    $this->transferVariation = $this->reloadEntity($transfer_variation);

    // Create uid: 1 here so that it's skipped in test cases.
    $admin_user = $this->createUser();
    $regular_user = $this->createUser(['uid' => 2]);
    \Drupal::currentUser()->setAccount($regular_user);
  }

  /**
   * @covers ::transferAccess
   */
  public function testVariationTransferAccess() {
    // Test access forbidden if transferring to product type that
    // doesn't allow variation type.
    $this->assertTrue($this->transferer->transferAccess($this->transferVariation, $this->product)->isForbidden());
    // Variations may not transfer to the product they currently belong to.
    $this->assertTrue($this->transferer->transferAccess($this->variation, $this->product)->isForbidden());
    // Test access forbidden if transferring to product type that
    // only allows one variation and the product already has a variation.
    $this->assertTrue($this->transferer->transferAccess($this->variation, $this->transferProduct)->isForbidden());
    // Test access allowed.
    $this->transferProduct->removeVariation($this->transferVariation)->save();
    $this->assertTrue($this->transferer->transferAccess($this->variation, $this->transferProduct)->isAllowed());
    // Test access if event modifies the access result.
    // The commerce_variation_transfer_test module does not allow
    // transferring to products with title 'Forbidden'.
    $this->transferProduct->setTitle('Forbidden')->save();
    $this->assertTrue($this->transferer->transferAccess($this->variation, $this->transferProduct)->isForbidden());
  }

  /**
   * @covers ::transfer
   */
  public function testTransfer() {
    // Test the variation and product references are set.
    $this->assertEquals($this->variation->getProductId(), $this->product->id());
    // Test the default product has one variation.
    $this->assertCount(1, $this->product->getVariationIds());
    // Test the transfer product has one variation.
    $this->assertCount(1, $this->transferProduct->getVariationIds());

    // Transfer the default variation to the transfer product.
    $this->transferer->transfer($this->variation, $this->transferProduct);

    // Refresh all the entities.
    $this->product = $this->reloadEntity($this->product);
    $this->variation = $this->reloadEntity($this->variation);
    $this->transferProduct = $this->reloadEntity($this->transferProduct);

    // Test the variation backreference to transfer product is set.
    $this->assertEquals($this->variation->getProductId(), $this->transferProduct->id());
    // Test the default product no longer references its default variation.
    $this->assertCount(0, $this->product->getVariationIds());
    // Test the transfer product references the variation transferred to it.
    $this->assertCount(2, $this->transferProduct->getVariationIds());
  }

  /**
   * Tests user access to the variation transfer operation.
   */
  public function testTransferOperationAccess() {
    // Access denied if user can manage variation type but
    // doesn't have transfer permission.
    $account = $this->drupalCreateUser(['manage default commerce_product_variation']);
    $this->assertFalse($this->variation->access('transfer', $account, FALSE));

    // Access denied if user has transfer permission but
    // can't manage variation type.
    $account = $this->drupalCreateUser(['transfer commerce_product_variation']);
    $this->assertFalse($this->variation->access('transfer', $account, FALSE));

    // Access allowed if user can manage variation type and
    // has transfer permission.
    $account = $this->drupalCreateUser([
      'manage default commerce_product_variation',
      'transfer commerce_product_variation',
    ]);
    $this->assertTrue($this->variation->access('transfer', $account, FALSE));
  }

}
