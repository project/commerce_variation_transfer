<?php

namespace Drupal\commerce_variation_transfer_test\EventSubscriber;

use Drupal\commerce_variation_transfer\Event\ProductVariationTransferAccessEvent;
use Drupal\commerce_variation_transfer\Event\ProductVariationTransferEvents;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to product variation transfer events.
 */
class ProductVariationTransferAccessSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      ProductVariationTransferEvents::PRODUCT_VARIATION_TRANSFER_ACCESS => ['onProductVariationTransferAccess'],
    ];
    return $events;
  }

  /**
   * Handle the product variation transfer access event.
   *
   * @param \Drupal\commerce_variation_transfer\Event\ProductVariationTransferAccessEvent $event
   *   The event.
   */
  public function onProductVariationTransferAccess(ProductVariationTransferAccessEvent $event) {
    if ($event->getNewProduct()->getTitle() === 'Forbidden') {
      $event->setAccess(AccessResult::forbidden('You shall not pass!'));
    }
  }

}
